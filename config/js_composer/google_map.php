<?php defined('ABSPATH') OR die('restricted access');

vc_map(array(
    'name'          => esc_html__( 'Google MAP', 'stamina-functions' ),
    'base'          => 'stamina_gmap',
    'icon'          => 'gmaps',
    'category'      => esc_html__( 'Stamina', 'stamina-functions' ),
    'params' => array(

        array(
            'type'          => 'textfield',
            'heading'       => esc_html__( 'Latitude', 'stamina-functions' ),
            'param_name'    => 'lat',
            'value'         => '51.516284',
            'group'         => esc_html__( 'Location', 'stamina-functions' )
        ),

        array(
            'type'          => 'textfield',
            'heading'       => esc_html__( 'Longitude', 'stamina-functions' ),
            'param_name'    => 'lng',
            'value'         => '-0.127637',
            'group'         => esc_html__( 'Location', 'stamina-functions' )
        ),

        array(
            'type'          => 'textfield',
            'heading'       => esc_html__( 'Map Name', 'stamina-functions'),
            'param_name'    => 'map_name',
            'admin_label'   => true,
            'value'         => 'Custom Map',
            'group'         => esc_html__( 'Styling', 'stamina-functions' )
        ),

        array(
            'type'          => 'textfield',
            'heading'       => esc_html__( 'Zoom', 'stamina-functions' ),
            'param_name'    => 'zoom',
            'value'         => '15',
            'group'         => esc_html__( 'Styling', 'stamina-functions' )
        ),

        array(
            'type'          => 'textfield',
            'heading'       => esc_html__( 'Height', 'stamina-functions' ),
            'param_name'    => 'map_height',
            'value'         => '400',
            'group'         => esc_html__( 'Styling', 'stamina-functions' ),
        ),

        array(
            'type'          => 'dropdown',
            'heading'       => esc_html__( 'Marker', 'stamina-functions' ),
            'param_name'    => 'marker',
            'value'         => array(
                esc_html__( 'Default', 'stamina-functions' ) => 'default',
                esc_html__( 'Custom', 'stamina-functions' ) => 'custom'
            ),

            'group'         => esc_html__( 'Styling', 'stamina-functions' ),
        ),

        array(
            'type'          => 'attach_image',
            'heading'       => esc_html__( 'Marker Image', 'stamina-functions' ),
            'param_name'    => 'marker_image',
            'value'         => '',
            'group'         => esc_html__( 'Styling', 'stamina-functions' ),
        ),
    )
));