<?php defined('ABSPATH') OR die('restricted access');

vc_map( array(
    'base'      => 'stamina_team_member',
    'name'      => esc_html__( 'Team Member', 'stamina-functions' ),
    'icon'      => 'icon-wpb-ui-separator-label',
    'category'  => esc_html__( 'Stamina', 'stamina-functions' ),
    'weight'    => 260,
    'params' => array(
        array(
            'param_name'        => 'image',
            'heading'           => esc_html__( 'Photo', 'stamina-functions' ),
            'description'       => '',
            'type'              => 'attach_image',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'weight'            => 150,
        ),

        array(
            'param_name'        => 'name',
            'heading'           => esc_html__( 'Name', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'weight'            => 130,
            'admin_label'       => true,
        ),
        array(
            'param_name'        => 'role',
            'heading'           => esc_html__( 'Role', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'weight'            => 120,
        ),
        array(
            'param_name'        => 'rss',
            'heading'           => esc_html__( 'Rss', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'weight'            => 100,
        ),
        array(
            'param_name'        => 'facebook',
            'heading'           => esc_html__( 'Facebook', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'weight'            => 90,
        ),
        array(
            'param_name'        => 'twitter',
            'heading'           => esc_html__( 'Twitter', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'weight'            => 80,
        ),
        array(
            'param_name'        => 'google_plus',
            'heading'           => esc_html__( 'Google+', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'weight'            => 70,
        ),
        array(
            'param_name'        => 'linkedin',
            'heading'           => 'LinkedIn',
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'weight'            => 60,
        ),
        array(
            'param_name'        => 'skype',
            'heading'           => esc_html__( 'Skype', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'weight'            => 50,
        ),
        array(
            'param_name'        => 'email',
            'heading'           => esc_html__( 'Email', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'weight'            => 100,
        ),

        array(
            'param_name'        => 'el_class',
            'heading'           => esc_html__( 'Extra class name', 'stamina-functions' ),
            'description'       => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'stamina-functions' ),
            'type'              => 'textfield',
            'weight'            => 10,
        ),
    ),
) );
