<?php defined('ABSPATH') OR die('restricted access');

$st_portfolio_cat = array();

$st_portfolio_cat_raw = get_categories( array(
    'taxonomy' => 'st_portfolio_cat',
) );

if ( $st_portfolio_cat_raw ) {
    foreach ( $st_portfolio_cat_raw as $portfolio_category ) {
        if ( is_object( $portfolio_category ) ) {
            $st_portfolio_cat[ $portfolio_category->name ] = $portfolio_category->slug;
        }
    }
}

vc_map( array(
    'base'      => 'stamina_portfolio',
    'name'      => esc_html__( 'Portfolio Grid', 'stamina-functions' ),
    'category'  => esc_html__( 'Stamina', 'stamina-functions' ),
    'params'    => array(

        array(
            'param_name'    => 'columns',
            'heading'       => esc_html__( 'Columns', 'stamina-functions' ),
            'type'          => 'dropdown',
            'value'         => array(
                esc_html__( '2 Columns', 'stamina-functions' )      => 'col-md-6 col-sm-6',
                esc_html__( '3 Columns', 'stamina-functions' )      => 'col-md-4 col-sm-6',
                esc_html__( '4 Columns', 'stamina-functions' )      => 'col-md-3 col-sm-6',
            ),

            'std'           => 'col-md-4 col-sm-6',
            'admin_label'   => TRUE,
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'orderby',
            'heading'       => esc_html__( 'Order By', 'stamina-functions' ),
            'type'          => 'dropdown',
            'value'         => array(
                esc_html__( 'ID', 'stamina-functions' ) => 'ID',
                esc_html__( 'Date', 'stamina-functions' ) => 'date',
                esc_html__( 'Random', 'stamina-functions' ) => 'rand',
                esc_html__( 'Comment Count', 'stamina-functions' ) => 'comment_count',
            ),
            'std'           => 'ID',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),

        array(
            'param_name'    => 'order',
            'heading'       => esc_html__( 'Order', 'stamina-functions' ),
            'type'          => 'dropdown',
            'value'         => array(
                esc_html__( 'DESC', 'stamina-functions' ) => 'DESC',
                esc_html__( 'ASC', 'stamina-functions' ) => 'ASC',
            ),
            'std'           => 'DESC',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),

        array(
            'param_name'    => 'posts_per_page',
            'heading'       => esc_html__( 'Items Per Page', 'stamina-functions' ),
            'type'          => 'textfield',
            'std'           => 6,
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'pagination',
            'heading'       => esc_html__( 'Pagination', 'stamina-functions' ),
            'type'          => 'dropdown',
            'value'         => array(
                esc_html__( 'No pagination', 'stamina-functions' )      => 'none',
                esc_html__( 'Regular pagination', 'stamina-functions' ) => 'navigation',
            ),
            'std'           => 'none',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),

        array(
            'param_name'    => 'filter',
            'heading'       => esc_html__( 'Filter', 'stamina-functions' ),
            'type'          => 'checkbox',
            'value'         => array( esc_html__( 'Enable filtering by category', 'us' ) => TRUE ),
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),

        array(
            'param_name'    => 'cat',
            'heading'       => esc_html__( 'Display Items of selected categories', 'stamina-functions' ),
            'type'          => 'checkbox',
            'value'         => $st_portfolio_cat,
        ),

        array(
            'param_name'    => 'el_class',
            'heading'       => esc_html__( 'Extra class name', 'stamina-functions' ),
            'description'   => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'stamina-functions' ),
            'type'          => 'textfield',
            'weight'        => 10,
        ),
    ),

) );