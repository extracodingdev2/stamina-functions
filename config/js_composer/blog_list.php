<?php defined('ABSPATH') OR die('restricted access');

$post_categories = array();
$categories = get_categories( "hierarchical=0" );
foreach ( $categories as $category ) {
    $post_categories[ $category->name ] = $category->slug;
}

vc_map( array(
    'base'          => 'stamina_blog_list',
    'name'          => esc_html__( 'Blog List', 'stamina-functions' ),
    'category'      => esc_html__( 'Stamina', 'stamina-functions' ),
    'weight'        => 240,
    'params'        => array(

        array(
            'param_name'    => 'content_type',
            'heading'       => esc_html__( 'Posts Content', 'stamina-functions' ),
            'type'          => 'dropdown',
            'value'         => array(
                esc_html__( 'Excerpt', 'stamina-functions' ) => 'excerpt',
                esc_html__( 'Full Content', 'stamina-functions' ) => 'content',
                esc_html__( 'None', 'stamina-functions' ) => 'none',
            ),
            'std'           => 'excerpt',
            'edit_field_class'=> 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'posts_per_page',
            'heading'       => esc_html__( 'Posts Per Page', 'stamina-functions' ),
            'type'          => 'textfield',
            'std'           => 3,
            'edit_field_class'=> 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'pagination',
            'heading'       => esc_html__( 'Pagination', 'stamina-functions' ),
            'type'          => 'dropdown',
            'value'         => array(
                esc_html__( 'No pagination', 'stamina-functions' )      => 'none',
                esc_html__( 'Regular pagination', 'stamina-functions' ) => 'navigation',
            ),
            'std'           => 'none',
            'edit_field_class'=> 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'show_date',
            'heading'       => esc_html__( 'Post Date', 'stamina-functions' ),
            'type'          => 'dropdown',
            'value'         => array(
                esc_html__( 'Show', 'stamina-functions' ) => 'show',
                esc_html__( 'Hide', 'stamina-functions' ) => 'hide',
            ),
            'std'           => 'show',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'show_author',
            'heading'       => esc_html__( 'Author', 'stamina-functions' ),
            'type'          => 'dropdown',
            'value'         => array(
                esc_html__( 'Show', 'stamina-functions' ) => 'show',
                esc_html__( 'Hide', 'stamina-functions' ) => 'hide',
            ),
            'std'           => 'show',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'show_read_more',
            'heading'       => esc_html__( 'Read More', 'stamina-functions' ),
            'type'          => 'dropdown',
            'value'         => array(
                esc_html__( 'Show', 'stamina-functions' ) => 'show',
                esc_html__( 'Hide', 'stamina-functions' ) => 'hide',
            ),
            'std'           => 'show',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'orderby',
            'heading'       => esc_html__( 'Order', 'stamina-functions' ),
            'type'          => 'dropdown',
            'value'         => array(
                esc_html__( 'By ID', 'stamina-functions' ) => 'ID',
                esc_html__( 'By Date', 'stamina-functions' ) => 'date',
                esc_html__( 'Random', 'stamina-functions' ) => 'rand',
                esc_html__( 'Comment Count', 'stamina-functions' ) => 'comment_count',
            ),
            'std'           => 'ID',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'cat',
            'heading'       => esc_html__( 'Display Posts of selected categories', 'stamina-functions' ),
            'type'          => 'checkbox',
            'value'         => $post_categories,
        ),
        array(
            'param_name'    => 'el_class',
            'heading'       => esc_html__( 'Extra class name', 'stamina-functions' ),
            'description'   => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'stamina-functions' ),
            'type'          => 'textfield',
        ),
    ),
) );
