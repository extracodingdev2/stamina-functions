<?php defined('ABSPATH') OR die('restricted access');

vc_map( array(
    'base'      => 'stamina_pricing_plan',
    'name'      => esc_html__( 'Pricing Table', 'stamina-functions' ),
    'category'  => esc_html__( 'Stamina', 'stamina-functions' ),
    'weight'    => 150,
    'params'    => array(

        array(
            'param_name'    => 'title',
            'heading'       => esc_html__( 'Item Title', 'stamina-functions' ),
            'type'          => 'textfield',
            'admin_label'   => true,
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'featured',
            'heading'       => esc_html__( 'Featured Item', 'stamina-functions' ),
            'type'          => 'dropdown',
            'value'         => array(
                esc_html__( 'Featured', 'stamina-functions' ) => 'premium',
                esc_html__( 'Normal', 'stamina-functions' )   => 'normal',
            ),
            'std'             => 'normal',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'        => 'price',
            'heading'           => esc_html__( 'Price', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'        => 'item_period',
            'heading'           => esc_html__( 'Time Period', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'features',
            'heading'       => esc_html__( 'Features List', 'stamina-functions' ),
            'type'          => 'textarea',
        ),
        array(
            'param_name'    => 'btn_text',
            'heading'       => esc_html__( 'Button Label', 'stamina-functions' ),
            'type'          => 'textfield',
            'class'         => 'wpb_button',
            'std'           => esc_html__( 'Order Now', 'stamina-functions' ),
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'btn_link',
            'heading'       => esc_html__( 'Button Link', 'stamina-functions' ),
            'type'          => 'textfield',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'btn_bg_color',
            'heading'       => esc_html__( 'Button Background Color', 'stamina-functions' ),
            'type'          => 'colorpicker',
            'std'           => '#0f8bcf',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),
        array(
            'param_name'    => 'btn_text_color',
            'heading'       => esc_html__( 'Button Text Color', 'stamina-functions' ),
            'type'          => 'colorpicker',
            'std'           => '#ffffff',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
        ),

        array(
            'param_name'    => 'el_class',
            'heading'       => esc_html__( 'Extra class name', 'stamina-functions' ),
            'description'   => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'stamina-functions' ),
            'type'          => 'textfield',
        ),
    ),

) );

