<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

vc_map( array(
    'base'          => 'stamina_clients_logos',
    'name'          => esc_html__( 'Clients Logos', 'stamina-functions' ),
    'category'      => esc_html__( 'Stamina', 'stamina-functions' ),
    'weight'        => 240,
    'params'        => array(

        array(
            'type'      => 'param_group',
            'heading'   => esc_html__( 'Clients Logo', 'stamina-functions' ),
            'param_name'=> 'items',
            'params'    => array(
                array(
                    'type'        => 'attach_images',
                    'heading'     => esc_html__( 'Images', 'stamina-functions' ),
                    'param_name'  => 'images',
                    'description' => esc_html__( 'Select images from media library.', 'stamina-functions' ),
                ),
            ),
        ),

        array(
            'param_name'    => 'el_class',
            'heading'       => esc_html__( 'Extra class name', 'stamina-functions' ),
            'description'   => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'stamina-functions' ),
            'type'          => 'textfield',
        ),
    ),
) );
