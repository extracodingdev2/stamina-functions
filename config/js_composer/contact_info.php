<?php defined('ABSPATH') OR die('restricted access');

vc_map( array(
    'name'              => esc_html__( 'Contact Info', 'stamina-functions' ),
    'base'              => 'stamina_contact_info',
    'category'          => esc_html__( 'Stamina', 'stamina-functions' ),
    'params'            => array(
        array(
            'param_name'        => 'icon',
            'heading'           => esc_html__( 'Icon', 'stamina-functions' ),
            'type'              => 'iconpicker',
            'value'             => 'fa fa-info-gear',
            'description'       => esc_html__( 'Select icon from library.', 'js_composer' ),
            'settings'          => array(
                'emptyIcon'     => false,
                'iconsPerPage'  => 4000,
            ),
        ),

        array(
            'param_name'        => 'email',
            'heading'           => esc_html__( 'Email', 'stamina-functions' ),
            'type'              => 'textfield',
            'admin_label'       => true,
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
        ),

        array(
            'param_name'        => 'alter_email',
            'heading'           => esc_html__( 'Alternate Email', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
        ),

        array(
            'param_name'        => 'address',
            'heading'           => esc_html__( 'Address', 'stamina-functions' ),
            'type'              => 'textfield',
            // 'edit_field_class'  => 'vc_col-sm-4 vc_column',
            'admin_label'       => true,
        ),

        array(
            'param_name'        => 'state',
            'heading'           => esc_html__( 'State', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
        ),

        array(
            'param_name'        => 'country',
            'heading'           => esc_html__( 'Country', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
        ),

        array(
            'param_name'        => 'mobile_number',
            'heading'           => esc_html__( 'Mobile Number', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'admin_label'       => true,
        ),

        array(
            'param_name'        => 'number',
            'heading'           => esc_html__( 'Phone Number', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
        ),

        array(
            'param_name'        => 'el_class',
            'heading'           => esc_html__( 'Extra class name', 'stamina-functions' ),
            'description'       => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'stamina-functions' ),
            'type'              => 'textfield',
        ),

        array(
            'param_name'        => 'icon_color',
            'heading'           => esc_html__( 'Icon Color', 'stamina-functions' ),
            'type'              => 'colorpicker',
            'std'               => '#0f8bcf',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'group'             => esc_html__( 'Style Options', 'stamina-functions' )
        ),

        array(
            'param_name'        => 'text_color',
            'heading'           => esc_html__( 'Text Color', 'stamina-functions' ),
            'type'              => 'colorpicker',
            'std'               => '#333333',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'group'             => esc_html__( 'Style Options', 'stamina-functions' )
        ),

        array(
            'param_name'            => 'icon_position',
            'heading'               => esc_html__( 'Icon Position', 'stamina-functions' ),
            'type'                  => 'dropdown',
            'value'                 => array(
                esc_html__( 'Left', 'stamina-functions' )   => 'left',
                esc_html__( 'Center', 'stamina-functions' ) => 'center',
                esc_html__( 'Right', 'stamina-functions' )  => 'right',
            ),
            'edit_field_class'      => 'vc_col-sm-6 vc_column',
            'std'                   => 'center',
            'group'                 => esc_html__( 'Style Options', 'stamina-functions' )
        ),

        array(
            'param_name'            => 'text_position',
            'heading'               => esc_html__( 'Text Position', 'stamina-functions' ),
            'type'                  => 'dropdown',
            'value'                 => array(
                esc_html__( 'Left', 'stamina-functions' )   => 'left',
                esc_html__( 'Center', 'stamina-functions' ) => 'center',
                esc_html__( 'Right', 'stamina-functions' )  => 'right',
            ),
            'edit_field_class'      => 'vc_col-sm-6 vc_column',
            'std'                   => 'center',
            'group'                 => esc_html__( 'Style Options', 'stamina-functions' )
        ),

    )
) );
