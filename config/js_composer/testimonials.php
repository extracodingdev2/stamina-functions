<?php defined('ABSPATH') OR die('restricted access');

vc_map( array(
    'base'      => 'stamina_testimonials',
    'name'      => esc_html__( 'Testimonials', 'stamina-functions' ),
    'category'  => esc_html__( 'Stamina', 'stamina-functions' ),
    'params' => array(

        array(
            'type'              => 'textfield',
            'heading'           => esc_html__( 'Items per page', 'stamina-functions' ),
            'param_name'        => 'posts_per_page',
            'description'       => esc_html__( 'Number of items to show per page.', 'stamina-functions' ),
            'value'             => '10',
            'admin_label'       => true,
        ),

        array(
            'type'          => 'dropdown',
            'heading'       => esc_html__( 'Order by', 'stamina-functions' ),
            'param_name'    => 'orderby',
            'value'         => array(
                esc_html__( 'Date', 'stamina-functions' ) => 'date',
                esc_html__( 'Order by post ID', 'stamina-functions' ) => 'ID',
                esc_html__( 'Author', 'stamina-functions' ) => 'author',
                esc_html__( 'Title', 'stamina-functions' ) => 'title',
                esc_html__( 'Last modified date', 'stamina-functions' ) => 'modified',
                esc_html__( 'Post/page parent ID', 'stamina-functions' ) => 'parent',
                esc_html__( 'Number of comments', 'stamina-functions' ) => 'comment_count',
                esc_html__( 'Menu order/Page Order', 'stamina-functions' ) => 'menu_order',
                esc_html__( 'Random order', 'stamina-functions' ) => 'rand',
            ),
            'description' => esc_html__( 'Select order type.', 'stamina-functions' ),
        ),

        array(
            'type'          => 'dropdown',
            'heading'       => esc_html__( 'Sort order', 'stamina-functions' ),
            'param_name'    => 'order',
            'value'         => array(
                esc_html__( 'Descending', 'stamina-functions' ) => 'DESC',
                esc_html__( 'Ascending', 'stamina-functions' ) => 'ASC',
            ),

            'description'   => esc_html__( 'Select sorting order.', 'stamina-functions' ),
        ),

        array(
            'param_name'        => 'el_class',
            'heading'           => esc_html__( 'Extra class name', 'stamina-functions' ),
            'description'       => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'stamina-functions' ),
            'type'              => 'textfield',
            'weight'            => 10,
        ),
    ),
) );
