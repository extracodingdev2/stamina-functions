<?php defined('ABSPATH') OR die('restricted access');

vc_map( array(
    'base'      => 'stamina_fun_fact',
    'name'      => esc_html__( 'Fun Fact', 'stamina-functions' ),
    'icon'      => 'icon-wpb-ui-separator',
    'category'  => esc_html__( 'Stamina', 'stamina-functions' ),
    'weight'    => 190,
    'params'    => array(

        array(
            'param_name'        => 'icon',
            'heading'           => esc_html__( 'Icon', 'stamina-functions' ),
            'type'              => 'iconpicker',
            'value'             => 'fa fa-info-circle',
            'description'       => esc_html__( 'Select icon from library.', 'js_composer' ),
            'settings'          => array(
                'emptyIcon'     => false,
                'iconsPerPage'  => 4000,
            ),
        ),

        array(
            'param_name'        => 'target_number',
            'heading'           => esc_html__( 'Number', 'stamina-functions' ),
            'type'              => 'textfield',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'weight'            => 80,
        ),

        array(
            'param_name'        => 'title',
            'heading'           => esc_html__( 'Title for Counter', 'stamina-functions' ),
            'type'              => 'textfield',
            'weight'            => 40,
            'admin_label'       => true,
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
        ),

        array(
            'param_name' => 'el_class',
            'heading' => esc_html__( 'Extra class name', 'stamina-functions' ),
            'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'stamina-functions' ),
            'type' => 'textfield',
            'weight' => 10,
        ),

        array(
            'param_name'        => 'icon_color',
            'heading'           => esc_html__( 'Icon Color', 'stamina-functions' ),
            'type'              => 'colorpicker',
            'weight'            => 90,
            'std'               => '#ffffff',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'group'             => esc_html__( 'Style Options', 'stamina-functions' )
        ),

        array(
            'param_name'        => 'text_color',
            'heading'           => esc_html__( 'Text Color', 'stamina-functions' ),
            'type'              => 'colorpicker',
            'std'               => '#ffffff',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'group'             => esc_html__( 'Style Options', 'stamina-functions' )
        ),

        array(
            'param_name'            => 'text_position',
            'heading'               => esc_html__( 'Text Position', 'stamina-functions' ),
            'type'                  => 'dropdown',
            'value'                 => array(
                esc_html__( 'Left', 'stamina-functions' )   => 'text-left',
                esc_html__( 'Center', 'stamina-functions' ) => 'text-center',
                esc_html__( 'Right', 'stamina-functions' )  => 'text-right',
            ),
            'edit_field_class'      => 'vc_col-sm-6 vc_column',
            'weight'                => 70,
            'std'                   => 'text-center',
            'group'                 => esc_html__( 'Style Options', 'stamina-functions' )
        ),
    ),
) );

