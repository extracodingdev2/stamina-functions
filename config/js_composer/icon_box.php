<?php defined('ABSPATH') OR die('restricted access');

vc_map( array(
    'name'              => esc_html__( 'Icon Box', 'stamina-functions' ),
    'base'              => 'stamina_icon_box',
    'icon'              => 'icon-wpb-ui-separator-label',
    'category'          => esc_html__( 'Stamina', 'stamina-functions' ),
    'params'            => array(
        array(
            'param_name'        => 'icon',
            'heading'           => esc_html__( 'Icon', 'stamina-functions' ),
            'type'              => 'iconpicker',
            'value'             => 'fa fa-info-gear',
            'description'       => esc_html__( 'Select icon from library.', 'js_composer' ),
            'settings'          => array(
                'emptyIcon'     => false,
                'iconsPerPage'  => 4000,
            ),
        ),

        array(
            'param_name'        => 'title',
            'heading'           => esc_html__( 'Title', 'stamina-functions' ),
            'type'              => 'textfield',
            'admin_label'       => true,
            'weight'            => 50,
        ),

        array(
            'param_name'        => 'iconbox_content',
            'heading'           => esc_html__( 'Iconbox content', 'stamina-functions' ),
            'type'              => 'textarea',
            'weight'            => 40,
        ),
        array(
            'param_name'        => 'el_class',
            'heading'           => esc_html__( 'Extra class name', 'stamina-functions' ),
            'description'       => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'stamina-functions' ),
            'type'              => 'textfield',
            'weight'            => 10,
        ),

        array(
            'param_name'        => 'icon_color',
            'heading'           => esc_html__( 'Icon Color', 'stamina-functions' ),
            'type'              => 'colorpicker',
            'weight'            => 90,
            'std'               => '#0f8bcf',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'group'             => esc_html__( 'Style Options', 'stamina-functions' )
        ),

        array(
            'param_name'            => 'icon_position',
            'heading'               => esc_html__( 'Icon Position', 'stamina-functions' ),
            'type'                  => 'dropdown',
            'value'                 => array(
                esc_html__( 'Left', 'stamina-functions' )   => 'left',
                esc_html__( 'Center', 'stamina-functions' ) => 'center',
                esc_html__( 'Right', 'stamina-functions' )  => 'right',
            ),
            'edit_field_class'      => 'vc_col-sm-6 vc_column',
            'weight'                => 70,
            'std'                   => 'center',
            'group'                 => esc_html__( 'Style Options', 'stamina-functions' )
        ),

        array(
            'param_name'        => 'text_color',
            'heading'           => esc_html__( 'Text Color', 'stamina-functions' ),
            'type'              => 'colorpicker',
            'std'               => '#333333',
            'edit_field_class'  => 'vc_col-sm-6 vc_column',
            'group'             => esc_html__( 'Style Options', 'stamina-functions' )
        ),

        array(
            'param_name'            => 'text_position',
            'heading'               => esc_html__( 'Text Position', 'stamina-functions' ),
            'type'                  => 'dropdown',
            'value'                 => array(
                esc_html__( 'Left', 'stamina-functions' )   => 'left',
                esc_html__( 'Center', 'stamina-functions' ) => 'center',
                esc_html__( 'Right', 'stamina-functions' )  => 'right',
            ),
            'edit_field_class'      => 'vc_col-sm-6 vc_column',
            'weight'                => 70,
            'std'                   => 'center',
            'group'                 => esc_html__( 'Style Options', 'stamina-functions' )
        ),

    )
) );
