<?php defined( 'ABSPATH' ) or die( 'restricted access' );

/*
Plugin Name:    Stamina Theme Functions
Plugin URI:     http://demo.designcarebd.com/stamina/
License:        http://www.extracoding.com/plugins/pro/license.txt
Description:    A required plugin for the Stamina theme you purchased from ThemeForest. It includes a number of features that you can still use if you switch to another theme.
Version:        1.0
Author:         Designcarebd
Author URI:     https://themeforest.net/user/theme_care/portfolio
Text Domain:    stamina-functions
*/

/**
* Define Stamina constants, if not already defined
*/
defined( 'STAMINA_FUNCTIONS_ABS_PATH' )
    || define( 'STAMINA_FUNCTIONS_ABS_PATH', plugin_dir_path( __FILE__ ) );

defined( 'STAMINA_FUNCTIONS_PLUGIN_URL' )
    || define( 'STAMINA_FUNCTIONS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );


function stamina_functions_initialize()
{
    load_plugin_textdomain( 'stamina-functions', false, basename( dirname( __FILE__ ) ) . '/languages' );
}

add_action( 'plugins_loaded', 'stamina_functions_initialize' );

/**
 * Load plugin
 *
 * @since 1.0.0
 * @return array
 */
function stamina_functions_load( $version = '' )
{
    require STAMINA_FUNCTIONS_ABS_PATH . '/plugin_class.php';
}

add_action( 'plugins_loaded', 'stamina_functions_load' );