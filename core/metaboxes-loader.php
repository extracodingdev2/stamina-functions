<?php defined('ABSPATH') OR die('restricted access');

if(!function_exists('stamina_register_custom_extension_loader')) :
    function stamina_register_custom_extension_loader($ReduxFramework) {
        $path = STAMINA_FUNCTIONS_ABS_PATH . '/extensions/';

        // $pat
        $folders = scandir( $path, 1 );
        foreach($folders as $folder) {
            if ($folder === '.' or $folder === '..' or !is_dir($path . $folder) ) {
                continue;
            }
            $extension_class = 'ReduxFramework_Extension_' . $folder;
            if( !class_exists( $extension_class ) ) {
                // In case you wanted override your override, hah.
                $class_file = $path . $folder . '/extension_' . $folder . '.php';
                $class_file = apply_filters( 'redux/extension/'.$ReduxFramework->args['opt_name'].'/'.$folder, $class_file );
                if( $class_file ) {
                    require_once( $class_file );
                    $extension = new $extension_class( $ReduxFramework );
                }
            }
        }
    }
    // Modify redux_demo to match your opt_name
    add_action("redux/extensions/stamina_options/before", 'stamina_register_custom_extension_loader', 0);
endif;