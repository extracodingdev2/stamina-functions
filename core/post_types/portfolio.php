<?php defined('ABSPATH') OR die('restricted access');

/**
 * Registers a new post type
 * @uses $wp_post_types Inserts new post type object into the list
 *
 * @param string  Post type key, must not exceed 20 characters
 * @param array|string  See optional args description above.
 * @return object|WP_Error the registered post type object, or an error object
 */
function stamina_register_portfolio_post_type() {

    $labels = array(
        'name'               => esc_html__( 'Portfolio', 'stamina-functions' ),
        'singular_name'      => esc_html__( 'Portfolio', 'stamina-functions' ),
        'add_new'            => esc_html_x( 'Add New Portfolio', 'stamina-functions', 'stamina-functions' ),
        'add_new_item'       => esc_html__( 'Add New Portfolio', 'stamina-functions' ),
        'edit_item'          => esc_html__( 'Edit Portfolio', 'stamina-functions' ),
        'new_item'           => esc_html__( 'New Portfolio', 'stamina-functions' ),
        'view_item'          => esc_html__( 'View Portfolio', 'stamina-functions' ),
        'search_items'       => esc_html__( 'Search Portfolio', 'stamina-functions' ),
        'not_found'          => esc_html__( 'No Portfolio found', 'stamina-functions' ),
        'not_found_in_trash' => esc_html__( 'No Portfolio found in Trash', 'stamina-functions' ),
        'parent_item_colon'  => esc_html__( 'Parent Portfolio:', 'stamina-functions' ),
        'menu_name'          => esc_html__( 'Portfolio', 'stamina-functions' ),
    );

    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'taxonomies'          => array('st_portfolio_cat'),
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => false,
        'query_var'           => true,
        'can_export'          => true,
        'rewrite'             => array('slug' => 'portfolio' ),
        'exclude_from_search' => true,
        'capability_type'     => 'post',
        'supports'            => array( 'title', 'thumbnail', 'excerpt', 'editor', 'author' ),
    );

    register_post_type( 'st_portfolio', $args );

    $labels = array(
        'name'                  => esc_html_x( 'Portfolio Categories', 'Taxonomy plural name', 'stamina-functions' ),
        'singular_name'         => esc_html_x( 'Categories', 'Taxonomy singular name', 'stamina-functions' ),
        'search_items'          => esc_html__( 'Search Plural Name', 'stamina-functions' ),
        'popular_items'         => esc_html__( 'Popular Plural Name', 'stamina-functions' ),
        'all_items'             => esc_html__( 'All Plural Name', 'stamina-functions' ),
        'parent_item'           => esc_html__( 'Parent Category Name', 'stamina-functions' ),
        'parent_item_colon'     => esc_html__( 'Parent Category Name', 'stamina-functions' ),
        'edit_item'             => esc_html__( 'Edit Category Name', 'stamina-functions' ),
        'update_item'           => esc_html__( 'Update Category Name', 'stamina-functions' ),
        'add_new_item'          => esc_html__( 'Add New Category Name', 'stamina-functions' ),
        'new_item_name'         => esc_html__( 'New Category Name Name', 'stamina-functions' ),
        'add_or_remove_items'   => esc_html__( 'Add or remove Plural Name', 'stamina-functions' ),
        'choose_from_most_used' => esc_html__( 'Choose from most used Plural Name', 'stamina-functions' ),
        'menu_name'             => esc_html__( 'Category', 'stamina-functions' ),
    );

    $args = array(
        'labels'            => $labels,
        'public'            => true,
        'show_admin_column' => false,
        'hierarchical'      => true,
        'show_tagcloud'     => true,
        'show_ui'           => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'portfolio-category'),
        'query_var'         => true,
        'capabilities'      => array(),
    );

    register_taxonomy( 'st_portfolio_cat', array('st_portfolio'), $args );
}

add_action( 'init', 'stamina_register_portfolio_post_type', 1 );
