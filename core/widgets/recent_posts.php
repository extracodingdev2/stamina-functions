<?php defined('ABSPATH') OR die('restricted access');

if ( ! function_exists( 'stamina_recent_posts_widget' ) ) {
    function stamina_recent_posts_widget() {
        register_widget( 'Nexum_Recent_Posts' );
    }
}
add_action( 'widgets_init', 'stamina_recent_posts_widget' );

if ( ! class_exists( 'Nexum_Recent_Posts' ) ) {

    class Nexum_Recent_Posts extends WP_Widget {

        function __construct() {

            $widget_ops = array( 'classname' => 'st-recent-posts', 'description' => esc_html__( 'Display the Recent Posts with images', 'nexum-plugin' ) );
            parent::__construct( 'st-recent-posts', esc_html__( 'Nexum Recent Posts', 'nexum-plugin' ), $widget_ops );
        }

        function widget( $args, $instance ) {

            extract( $args );

            $title = isset( $instance['title'] ) ? $instance['title'] : esc_html__( 'Recent Posts', 'nexum-plugin' );
            $orderby = isset( $instance['orderby'] ) ? $instance['orderby'] : 'ID';
            $order = isset( $instance['order'] ) ? $instance['order'] : 'DESC';
            $posts_per_page = isset( $instance['posts_per_page'] ) ? absint( $instance['posts_per_page'] ) : '4';

            $atts = array(
                'post_type'         => 'post',
                'posts_per_page'    => $posts_per_page,
                'post_status'       => 'publish',
                'order'             => $order,
                'orderby'           => $orderby,
                'ignore_sticky_posts'=> true,
            );

            echo html_entity_decode( $before_widget );

                if ( ! empty( $title ) ) {
                    echo $before_title . '<h3>'.$title .'</h3>'. $after_title;
                }

                $query = new WP_Query( $atts );

                if ( $query->have_posts() ) : ?>
                        <div class="widget-recent-posts">
                            <ul>
                                <?php while( $query->have_posts() ): $query->the_post();?>

                                    <li>
                                        <?php if ( has_post_thumbnail() ) {
                                            the_post_thumbnail('stamina-recent-post-widget');
                                        }; ?>

                                        <a class="post-title" href="<?php the_permalink(); ?>">
                                            <?php echo substr( get_the_title(), 0, 15 ); ?>
                                        </a>

                                        <div class="post-details">
                                            <span>
                                                <i class="fa fa-calendar"></i>
                                                <?php echo get_the_date( 'M d' ); ?>
                                            </span>
                                            <span><i class="fa fa-user"></i> <?php echo get_the_author(); ?></span>
                                        </div><!-- post-details -->
                                    </li>

                                <?php endwhile; ?>

                            </ul>
                        </div>
                <?php endif;

            echo html_entity_decode( $after_widget );

        }

        function update( $new_instance, $old_instance ) {

            $instance = $old_instance;

            $instance['title'] = isset( $new_instance['title'] ) ? sanitize_text_field( $new_instance['title'] ) : esc_html__( 'Recent Posts', 'nexum-plugin' );
            $instance['orderby'] = isset( $_POST['orderby'] ) ? $_POST['orderby'] : 'ID';
            $instance['order'] = isset( $_POST['order'] ) ? $_POST['order'] : 'DESC';
            $instance['posts_per_page'] = isset( $new_instance['posts_per_page'] ) ? absint( $new_instance['posts_per_page'] ) : '4';

            return $instance;

        }

        function form( $instance ) {

            $title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : esc_html__( 'Recent Posts', 'nexum-plugin' );
            $orderby = isset( $instance['orderby'] ) ? esc_attr( $instance['orderby'] ) : 'ID';
            $order = isset( $instance['order'] ) ? esc_attr( $instance['order'] ) : 'DESC';
            $posts_per_page = isset( $instance['posts_per_page'] ) ? absint( $instance['posts_per_page'] ) : '4';

            ?>

            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'nexum-plugin' ); ?></label>
                <br/><input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $title ); ?>" />
            </p>

            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>"><?php esc_html_e( 'Order By:', 'nexum-plugin' ); ?></label>
                <br/><select id="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>" name="orderby" class="widefat">
                    <option value="ID" <?php if ( $orderby == 'ID' ) { echo 'selected="selected"'; } ?>><?php esc_html_e( 'Newest', 'nexum-plugin' ); ?></option>
                    <option value="author" <?php if ( $orderby == 'author' ) { echo 'selected="selected"'; } ?>><?php esc_html_e( 'Author', 'nexum-plugin' ); ?></option>
                    <option value="comment_count" <?php if ( $orderby == 'comment_count' ) { echo 'selected="selected"'; } ?>><?php esc_html_e( 'Most Comments', 'nexum-plugin' ); ?></option>
                    <option value="menu_order" <?php if ( $orderby == 'menu_order' ) { echo 'selected="selected"'; } ?>><?php esc_html_e( 'Menu Order', 'nexum-plugin' ); ?></option>
                    <option value="rand" <?php if ( $orderby == 'rand' ) { echo 'selected="selected"'; } ?>><?php esc_html_e( 'Random', 'nexum-plugin' ); ?></option>
                </select>
            </p>

            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>"><?php esc_html_e( 'Order:', 'nexum-plugin' ); ?></label>
                <br/><select id="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>" name="order" class="widefat">
                    <option value="ASC" <?php if ( $orderby == 'ASC' ) { echo 'selected="selected"'; } ?>><?php esc_html_e( 'Ascending', 'nexum-plugin' ); ?></option>
                    <option value="DESC" <?php if ( $orderby == 'DESC' ) { echo 'selected="selected"'; } ?>><?php esc_html_e( 'Descending', 'nexum-plugin' ); ?></option>
                </select>
            </p>

            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'posts_per_page' ) ); ?>"><?php esc_html_e( 'Per Page:', 'nexum-plugin' ); ?></label>
                <br/><input type="text" id="<?php echo esc_attr( $this->get_field_id( 'post_per_page' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'post_per_page' ) ) ?>" value="<?php echo esc_attr( $posts_per_page ); ?>" size="3" class="widefat" />
            </p>

        <?php
        }

    }
}