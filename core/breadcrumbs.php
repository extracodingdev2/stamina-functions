<?php defined('ABSPATH') OR die('restricted access');

if ( ! function_exists( 'stamina_breadcrumbs' ) )
{
    function stamina_breadcrumbs()
    {
        $crumb['home']     = esc_html__( 'Home', 'nexum-plugin' );
        $crumb['category'] = esc_html__( 'Archive by Category', 'nexum-plugin' ) . "%s";
        $crumb['tax']      = esc_html__( 'Archive for', 'nexum-plugin' ) . "%s";
        $crumb['search']   = 'Search Results for "%s" Query';
        $crumb['tag']      = esc_html__( 'Posts Tagged ', 'nexum-plugin' ) . "%s";
        $crumb['author']   = esc_html__( 'Articles Posted by ', 'nexum-plugin' ) . "%s";
        $crumb['404']      = esc_html__( 'Error 404 ', 'nexum-plugin' ) . "%s";

        $show_current= 1;
        $show_home   = 1;
        $delimiter   = ' / ';
        $before      = '<span class="current">';
        $after       = '</span>';

        global $post;

        $home_url = get_bloginfo( 'url' ) . '/';

        $link = '<a href="%1$s">%2$s</a>';

        if ( is_home() || is_front_page() ) {

            if ( $show_home == 1 ) {
                echo '<div id="crumbs"><a href="' . $home_url . '">' . $crumb['home'] . '</a></div>';
            }

        } else {

            global $post;

            echo '<div id="crumbs">' . sprintf( $link, $home_url, $crumb['home'] ) . $delimiter;

            if ( is_tax() || is_tag() || is_category() ) {

                $term = get_category( get_query_var( 'cat' ), false );

                if ( $term->parent != 0 ) {
                    $cats = get_category_parents( $term->parent, TRUE, $delimiter );
                    $cats = str_replace( '<a', '<a', $cats );
                    $cats = str_replace( '</a>', '</a>', $cats );

                    echo $cats;
                }

                echo $before . sprintf( $crumb['category'], single_cat_title( '', false ) ) . $after;

            } elseif ( is_author() ) {

                global $author;

                $user_data = get_userdata( $author );

                echo $before . sprintf( $crumb['author'] , $user_data->display_name ) . $after;

            } elseif ( is_search() ) {

                echo $before . sprintf( $crumb['search'], esc_html( get_search_query() ) ) . $after;

            } elseif ( is_day() ) {

                echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
                echo sprintf( $link, get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ), get_the_time( 'F' ) ) . $delimiter;
                echo $before . get_the_time('d') . $after;

            } elseif ( is_month() ) {

                echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
                echo $before . get_the_time('F') . $after;

            } elseif ( is_year() ) {
                echo $before . get_the_time('Y') . $after;

            } elseif ( is_page() ) {

                if ( $post->post_parent ) {

                    $ancestors = get_post_ancestors( $post->ID );

                    $ancestors = array_reverse( $ancestors );

                    if ( ! isset( $parents ) ) {
                        $parents = null;
                    }

                    foreach ( $ancestors as $ancestor ) {
                        echo '<a href="' . get_permalink( $ancestor ) . '">' . get_the_title( $ancestor ) . '</a>' . $delimiter ;
                    }

                    // Current page
                    echo $before . get_the_title() . $after;

                } else {

                    if ( $show_current == 1 ) {
                        echo $before . get_the_title() . $after;
                    }
                }

            } elseif ( is_single() ) {

                $archive_link = get_post_type_archive_link( $post->post_type );

                if ( isset( $post->post_type ) && $archive_link ) {

                    $obj = get_post_type_object( $post->post_type );

                    $slug = $obj->rewrite;

                    printf( $link, $home_url . $slug['slug'] . '/', $obj->labels->singular_name );

                    if ( $show_current == 1 ) {
                        echo $delimiter . $before . get_the_title() . $after;
                    }
                }

            } elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' && ! is_404() ) {

                $post_type = get_post_type_object( get_post_type() );

                echo $before . $post_type->labels->singular_name . $after;

            } elseif ( is_attachment() ) {

                $parent = get_post( $post->post_parent );
                $cat = get_the_category( $parent->ID ); $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, $delimiter );
                $cats = str_replace('<a', '<a', $cats );
                $cats = str_replace('</a>', '</a>', $cats );
                echo $cats;
                printf( $link, get_permalink( $parent ), $parent->post_title );

                if ( $show_current == 1 ) {
                    echo $delimiter . $before . get_the_title() . $after;
                }

            } elseif ( is_page() && ! $post->post_parent ) {

                if ( $show_current == 1) {
                    echo $before . get_the_title() . $after;
                }

            } elseif ( is_page() && $post->post_parent ) {
                $parent_id  = $post->post_parent;
                $breadcrumbs = array();

                while ( $parent_id ) {
                    $page = get_page( $parent_id );
                    $breadcrumbs[] = sprintf( $link, get_permalink( $page->ID ), get_the_title( $page->ID ) );
                    $parent_id  = $page->post_parent;
                }

                $breadcrumbs = array_reverse( $breadcrumbs );

                for ( $i = 0; $i < count( $breadcrumbs ); $i++ ) {

                    echo $breadcrumbs[$i];

                    if ( $i != count($breadcrumbs) - 1 ) {
                        echo $delimiter;
                    }
                }

                if ( $show_current == 1 ) {
                    echo $delimiter . $before . get_the_title() . $after;
                }

            }  elseif ( is_404() ) {
                echo $before . $crumb['404'] . $after;
            }

            echo '</div>';

        }
    }
}