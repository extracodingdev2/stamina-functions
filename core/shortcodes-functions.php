<?php defined('ABSPATH') OR die('restricted access');

if ( ! function_exists( 'stamina_icon_box_inline_css' ) ) {
    function stamina_icon_box_inline_css( $name = '', $icon_color = '', $icon_position = '', $text_color = '', $text_position = '' ) {

        $css = '';

        if ( $icon_color ) {
            $css .= '.' . sanitize_html_class( $name ) . ' i.fa{color:' . esc_attr( $icon_color ) . ';}';
        }

        if ( $icon_position ) {
            $css .= '.' . sanitize_html_class( $name ) . ' i.fa{text-align:' . esc_attr( $icon_position ) . ';}';
        }

        echo '<style>' . $css . '</style>';
    }
}

if ( ! function_exists( 'stamina_fun_fact_inline_css' ) ) {
    function stamina_fun_fact_inline_css( $name = '', $icon_color = '', $text_color = '', $text_position = '' ) {

        $css = '';

        if ( $icon_color ) {
            $css .= '.' . sanitize_html_class( $name ) . ' {color:' . esc_attr( $icon_color ) . ';}';
        }

        if ( $text_position ) {
            $css .= '.' . sanitize_html_class( $name ) . ' {text-align:' . esc_attr( $text_position ) . ';}';
        }

        if ( $text_color ) {
            $css .= '.' . sanitize_html_class( $name ) . ' h3{color:' . esc_attr( $text_color ) . ';}';
            $css .= '.' . sanitize_html_class( $name ) . ' p{color:' . esc_attr( $text_color ) . ';}';
        }

        echo '<style>' . $css . '</style>';
    }
}

if ( ! function_exists( 'stamina_pricing_plan_inline_css' ) ) {
    function stamina_pricing_plan_inline_css( $name = '', $btn_bg_color = '', $btn_text_color = '' ) {

        $css = '';

        if ( $btn_bg_color ) {
            $css .= '.' . sanitize_html_class( $name ) . ' a.btn{background:' . esc_attr( $btn_bg_color ) . ';}';
        }

        if ( $btn_text_color ) {
            $css .= '.' . sanitize_html_class( $name ) . ' a.btn{color:' . esc_attr( $btn_text_color ) . ';}';
        }

        echo '<style>' . $css . '</style>';
    }
}

if ( ! function_exists( 'stamina_team_social_links' ) )
{
    function stamina_team_social_links( $rss = '', $facebook = '', $twitter = '', $google_plus = '', $linkedin = '', $skype = '', $email = '' )
    {
        if ( $rss OR $facebook OR $twitter OR $google_plus OR $linkedin OR $skype OR $email ) : ?>
            <div class="team-hover">
                <ul>
                    <?php if ( ! empty( $rss ) ): ?>
                        <li><a href="<?php echo esc_url( $rss ); ?>"><i class="fa fa-rss"></i></a></li>
                    <?php endif;?>
                    <?php if ( ! empty( $facebook ) ): ?>
                        <li><a href="<?php echo esc_url( $facebook ); ?>"><i class="fa fa-facebook"></i></a></li>
                    <?php endif;?>
                    <?php if ( ! empty( $twitter ) ): ?>
                        <li><a href="<?php echo esc_url( $twitter ); ?>"><i class="fa fa-twitter"></i></a></li>
                    <?php endif;?>
                    <?php if ( ! empty( $google_plus ) ): ?>
                        <li><a href="<?php echo esc_url( $google_plus ); ?>"><i class="fa fa-google-plus"></i></a></li>
                    <?php endif;?>
                    <?php if ( ! empty( $linkedin ) ): ?>
                        <li><a href="<?php echo esc_url( $linkedin ); ?>"><i class="fa fa-linkedin"></i></a></li>
                    <?php endif;?>
                    <?php if ( ! empty( $skype ) ): ?>
                        <li><a href="<?php echo esc_url( $skype ); ?>"><i class="fa fa-skype"></i></a></li>
                    <?php endif;?>
                    <?php if ( ! empty( $email ) ): ?>
                        <li><a href="<?php echo esc_url( $email ); ?>"><i class="fa fa-envelope"></i></a></li>
                    <?php endif;?>
                </ul>
            </div>

        <?php
        endif;
    }
}