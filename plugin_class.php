<?php defined('ABSPATH') OR die('restricted access');

if ( ! class_exists( 'Stamina_Functions' ) )
{
    class Stamina_Functions
    {
        public function __construct()
        {
            require STAMINA_FUNCTIONS_ABS_PATH . '/core/shortcodes-functions.php';

            // Register Testimonail Post Types
            require STAMINA_FUNCTIONS_ABS_PATH . '/core/post_types/testimonials.php';

            // Register Potfolio Post Type
            require STAMINA_FUNCTIONS_ABS_PATH . '/core/post_types/portfolio.php';

            // Load Metabox Extension
            require STAMINA_FUNCTIONS_ABS_PATH . '/core/metaboxes-loader.php';

            // Load Breadcrumbs
            require STAMINA_FUNCTIONS_ABS_PATH . '/core/breadcrumbs.php';

            // Register Widgets
            // Recent Posts With Images
            require STAMINA_FUNCTIONS_ABS_PATH . '/core/widgets/recent_posts.php';

            add_action( 'vc_before_init', array( &$this, 'stamina_load_shortcodes' ) );
            add_action( 'vc_before_init', array( &$this, 'stamina_vc_shortcodes_config' ) );

            add_action( 'admin_enqueue_scripts', array( &$this, 'stamina_redux_theme_options' ) );
        }

        public function stamina_redux_theme_options()
        {

        }

        public function stamina_vc_shortcodes_config()
        {
            if ( function_exists( 'vc_set_as_theme' ) ) {
                require_once( sprintf( "%s/icon_box.php", STAMINA_FUNCTIONS_ABS_PATH . '/config/js_composer' ) );
                require_once( sprintf( "%s/fun_fact.php", STAMINA_FUNCTIONS_ABS_PATH . '/config/js_composer' ) );
                require_once( sprintf( "%s/team_member.php", STAMINA_FUNCTIONS_ABS_PATH . '/config/js_composer' ) );
                require_once( sprintf( "%s/pricing_plan.php", STAMINA_FUNCTIONS_ABS_PATH . '/config/js_composer' ) );
                require_once( sprintf( "%s/testimonials.php", STAMINA_FUNCTIONS_ABS_PATH . '/config/js_composer' ) );
                require_once( sprintf( "%s/clients_logos.php", STAMINA_FUNCTIONS_ABS_PATH . '/config/js_composer' ) );
                require_once( sprintf( "%s/contact_info.php", STAMINA_FUNCTIONS_ABS_PATH . '/config/js_composer' ) );
                require_once( sprintf( "%s/portfolio.php", STAMINA_FUNCTIONS_ABS_PATH . '/config/js_composer' ) );
                require_once( sprintf( "%s/google_map.php", STAMINA_FUNCTIONS_ABS_PATH . '/config/js_composer' ) );
                require_once( sprintf( "%s/blog_grid.php", STAMINA_FUNCTIONS_ABS_PATH . '/config/js_composer' ) );
                require_once( sprintf( "%s/blog_list.php", STAMINA_FUNCTIONS_ABS_PATH . '/config/js_composer' ) );
            }
        }

        public function stamina_load_shortcodes()
        {
            if ( function_exists( 'vc_set_as_theme' ) )
            {
                require_once( sprintf( "%s/icon_box.php", STAMINA_FUNCTIONS_ABS_PATH . '/views/shortcodes' ) );
                require_once( sprintf( "%s/fun_fact.php", STAMINA_FUNCTIONS_ABS_PATH . '/views/shortcodes' ) );
                require_once( sprintf( "%s/team_member.php", STAMINA_FUNCTIONS_ABS_PATH . '/views/shortcodes' ) );
                require_once( sprintf( "%s/pricing_plan.php", STAMINA_FUNCTIONS_ABS_PATH . '/views/shortcodes' ) );
                require_once( sprintf( "%s/testimonials.php", STAMINA_FUNCTIONS_ABS_PATH . '/views/shortcodes' ) );
                require_once( sprintf( "%s/clients_logos.php", STAMINA_FUNCTIONS_ABS_PATH . '/views/shortcodes' ) );
                require_once( sprintf( "%s/contact_info.php", STAMINA_FUNCTIONS_ABS_PATH . '/views/shortcodes' ) );
                require_once( sprintf( "%s/portfolio.php", STAMINA_FUNCTIONS_ABS_PATH . '/views/shortcodes' ) );
                require_once( sprintf( "%s/google_map.php", STAMINA_FUNCTIONS_ABS_PATH . '/views/shortcodes' ) );
                require_once( sprintf( "%s/blog_grid.php", STAMINA_FUNCTIONS_ABS_PATH . '/views/shortcodes' ) );
                require_once( sprintf( "%s/blog_list.php", STAMINA_FUNCTIONS_ABS_PATH . '/views/shortcodes' ) );
            }
        }
    }
}

$stamina_functions = new Stamina_Functions();