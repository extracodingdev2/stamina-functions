<?php if ( ! function_exists( 'stamina_contact_info_shortcode' ) )
{
    function stamina_contact_info_shortcode( $atts )
    {
        extract( shortcode_atts( array(
            'icon'          => '',
            'email'         => '',
            'alter_email'   => '',
            'address'       => '',
            'state'         => '',
            'country'       => '',
            'mobile_number' => '',
            'number'        => '',
            'icon_color'    => '#0f8bcf',
            'icon_position' => 'center',
            'text_color'    => '#333333',
            'text_position' => 'center',
            'el_class'      => '',
        ), $atts ) );

        if ( function_exists( 'stamina_icon_box_inline_css' ) ) {
            stamina_icon_box_inline_css( 'contact-info', $icon_color, $icon_position, $text_color, $text_position );
        }

        // Classes
        $css_classes = array(
            'contact-info',
            $icon_position,
            $text_position,
            $el_class,
        );

        $classes = trim( implode( ' ', array_filter( array_unique( $css_classes ) ) ) );

        ob_start(); ?>

        <div class="<?php echo esc_attr( $classes ); ?>">
            <?php if ( ! empty( $icon ) ): ?>
                <i class="<?php echo esc_attr( $icon ); ?>"></i>
            <?php endif; ?>
            <?php if ( ! empty( $email ) OR ! empty( $alter_email ) ): ?>
                <p>
                    <?php if ( ! empty( $email ) ): ?>
                        <a href="mailto:<?php echo esc_attr( $email ); ?>" title="<?php echo esc_attr__( 'Click to mail', 'stamina-functions' ); ?>" ><?php echo esc_html( $email ); ?></a>
                    <?php endif; ?>

                    <?php if ( ! empty( $alter_email ) ): ?>
                        <br />
                        <a href="mailto:<?php echo esc_attr( $alter_email ); ?>" title="<?php echo esc_attr__( 'Click to mail', 'stamina-functions' ); ?>" ><?php echo esc_html( $alter_email ); ?></a>
                    <?php endif; ?>
                </p>
            <?php endif; ?>

            <?php if ( ! empty( $address ) ): ?>
                <p>
                    <?php if ( ! empty( $address ) ): ?>
                        <?php echo esc_html( $address ); ?>
                    <?php endif; ?>

                    <?php if ( ! empty( $state ) ): ?>
                        <br />
                        <?php echo esc_html( $state ); ?>
                    <?php endif; ?>

                    <?php if ( ! empty( $country ) ): ?>
                        <?php echo esc_html( $country ); ?>
                    <?php endif; ?>
                </p>
            <?php endif; ?>

            <?php if ( ! empty( $mobile_number ) OR ! empty( $number ) ): ?>
                <p>
                    <?php if ( ! empty( $mobile_number ) ): ?>
                        <a href="tel:<?php echo esc_attr( $mobile_number ); ?>" title="<?php echo esc_attr__( 'Click to Call', 'stamina-functions' ); ?>" ><?php echo esc_html( $mobile_number ); ?></a>
                    <?php endif; ?>

                    <?php if ( ! empty( $number ) ): ?>
                        <br />
                        <a href="tel:<?php echo esc_attr( $number ); ?>" title="<?php echo esc_attr__( 'Click to Call', 'stamina-functions' ); ?>" ><?php echo esc_html( $number ); ?></a>
                    <?php endif; ?>
                </p>
            <?php endif; ?>
        </div>

        <?php

        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;

    }
}

add_shortcode( 'stamina_contact_info', 'stamina_contact_info_shortcode' ); ?>