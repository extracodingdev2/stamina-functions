<?php if ( ! function_exists( 'stamina_clients_logos_shortcode' ) )
{
    function stamina_clients_logos_shortcode( $atts )
    {
        extract( shortcode_atts( array(
            'items'             => '',
            'el_class'          => '',
        ), $atts ) );

        if ( empty( $items ) ) {
            $items = array();
        } else {
            $items = json_decode( urldecode( $items ), TRUE );
            if ( ! is_array( $items ) ) {
                $items = array();
            }
        }

        // Classes
        $classes = array(
            'owl-client',
            $el_class,
        );

        $classes = trim( implode( ' ', array_filter( array_unique( $classes ) ) ) );

        ob_start(); ?>

        <div class="<?php echo esc_attr( $classes ); ?>">
            <?php
            foreach ( $items as $index => $attachemnt_id ) :

                if ( empty( $attachemnt_id ) && ! $attachemnt_id['images'] ) {
                    continue;
                }

                ?>

                <div class="item text-center">
                    <?php echo wp_get_attachment_image( $attachemnt_id['images'], 'stamina-clients-logo' ); ?>
                </div>

            <?php endforeach; ?>
        </div>

    <?php
        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;
    }
}

add_shortcode( 'stamina_clients_logos', 'stamina_clients_logos_shortcode' ); ?>