<?php if ( ! function_exists( 'stamina_fun_fact_shortcode' ) )
{
    function stamina_fun_fact_shortcode( $atts )
    {
        extract( shortcode_atts( array(
            'icon'          => '',
            'target_number' => '',
            'title'         => '',
            'icon_color'    => '#ffffff',
            'text_color'    => '',
            'text_position' => 'center',
            'el_class'      => '',
        ), $atts ) );

        if ( function_exists( 'stamina_fun_fact_inline_css' ) ) {
            stamina_fun_fact_inline_css( 'fun-fact', $icon_color, $text_color, $text_position );
        }

        // Classes
        $css_classes = array(
            'fun-fact',
            $text_position,
            $el_class,
        );

        $classes = trim( implode( ' ', array_filter( array_unique( $css_classes ) ) ) );

        ob_start(); ?>

        <div class="<?php echo esc_attr( $classes ); ?>">
            <?php if ( ! empty( $icon ) ): ?>
                <i class="<?php echo esc_attr( $icon ); ?>"></i>
            <?php endif; ?>

            <?php if ( ! empty( $target_number ) ): ?>
                <h3><span class="timer"><?php echo esc_html( $target_number ); ?></span></h3>
            <?php endif; ?>
            <?php if ( ! empty( $title ) ): ?>
                <p><?php echo esc_html( $title ); ?></p>
            <?php endif; ?>
        </div>

        <?php

        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;

    }
}

add_shortcode( 'stamina_fun_fact', 'stamina_fun_fact_shortcode' ); ?>