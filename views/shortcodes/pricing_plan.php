<?php if ( ! function_exists( 'stamina_pricing_plan_shortcode' ) )
{
    function stamina_pricing_plan_shortcode( $atts )
    {
        extract( shortcode_atts( array(
            'title'             => '',
            'featured'          => '',
            'price'             => '',
            'item_period'       => '',
            'features'          => '',
            'btn_text'          => esc_html__( 'Order Now', 'stamina-functions' ),
            'btn_link'          => '#',
            'btn_bg_color'      => '#0f8bcf',
            'btn_text_color'    => '',
            'el_class'          => '',
        ), $atts ) );

        if ( function_exists( 'stamina_pricing_plan_inline_css' ) ) {
            stamina_pricing_plan_inline_css( 'single-item-table', $btn_bg_color, $btn_text_color );
        }

        // Classes
        $classes = array(
            'table-heading',
            $featured,
            $el_class,
        );

        $classes = trim( implode( ' ', array_filter( array_unique( $classes ) ) ) );

        ob_start(); ?>

        <div class="single-item-table">

            <?php if ( ! empty( $title ) ): ?>
                <div class="<?php echo esc_html( $classes ); ?>">
                     <p class="plan"><?php echo esc_html( $title ); ?></p>
                </div>
            <?php endif; ?>
            <?php if ( ! empty( $price ) ): ?>
                <div class="rate">
                     <span class="number"><?php echo esc_html( $price ); ?></span>
                      <?php if ( ! empty( $item_period ) ): ?>
                          <span>/ <?php echo esc_html( $item_period ); ?></span>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <?php if ( ! empty( $features ) ): ?>
                <div class="description">
                    <ul>
                        <?php $feature_list = explode( "\n", trim( $features ) );
                        foreach ( (array) $feature_list as $feature ) {
                            echo '<li>' . $feature . '</li>';
                        } ?>
                    </ul>
                </div>
            <?php endif; ?>
            <?php if ( ! empty( $btn_text ) && ! empty( $btn_link ) ): ?>
                <a class="btn" href="<?php echo esc_url( $btn_link ); ?>">
                    <?php echo esc_html( $btn_text ); ?>
                </a>
            <?php endif; ?>
        </div>

        <?php

        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;

    }
}

add_shortcode( 'stamina_pricing_plan', 'stamina_pricing_plan_shortcode' ); ?>