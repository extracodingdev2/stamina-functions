<?php if ( ! function_exists( 'stamina_google_map_shortcode' ) )
{
    function stamina_google_map_shortcode( $atts )
    {
        $add_height = '';

        extract(shortcode_atts( array(
            'lat'               => '51.516284',
            'lng'               => '-0.127637',
            'map_name'          => 'Custom Map',
            'zoom'              => 15,
            'map_height'        => '400',
            'el_class'          => '',
            'marker'            => 'default',
            'marker_image'      => '',
        ), $atts ) );

        if ( empty( stamina_option( 'map_api' ) ) ) {
            return;
        }

        $map_name = "google_map_" . uniqid();

        if ( $map_height != '' ) {
            $add_height = 'height: '. $map_height . 'px;';
        }

        // Classes
        $css_classes = array(
            $map_name,
            $el_class,
        );

        $classes = trim( implode( ' ', array_filter( array_unique( $css_classes ) ) ) );

        $icon = get_template_directory_uri() . '/assets/img/map-marker.png';

        if ( ! empty( $marker ) && $marker == 'custom' && ! empty( $marker_image ) ) {

            $icon = wp_get_attachment_image_url( $marker_image );
        }

        $output = '

        <script type="text/javascript">
            var places = [['.$lat.', '.$lng.', 1]];

            function initialize()
            {
                var mapOptions =
                {
                    scrollwheel: false,
                    zoom: '.$zoom.',
                    center: new google.maps.LatLng('.$lat.', '.$lng.'),
                    mapTypeId:google.maps.MapTypeId.ROADMAP
                }

                var map = new google.maps.Map(document.getElementById("'.$map_name.'"), mapOptions);

                setMarkers(map, places);
            }

            function setMarkers(map, locations)
            {
                for (var i = 0; i < locations.length; i++)
                {
                    var place = locations[i];
                    var myLatLng = new google.maps.LatLng(place[0], place[1]);
                    var marker = new google.maps.Marker(
                    {
                        position: myLatLng,
                        map: map,
                        icon:"'.$icon.'",
                        zIndex: place[2],
                        animation: google.maps.Animation.BOUNCE
                    });

                    var styles = [
                        {
                            stylers: [
                                { hue: "#c5c5c5" },
                                { saturation: -100 }
                            ]
                        },
                    ];

                    map.setOptions({styles: styles});
                    marker.setMap(map);
                }
            }

            google.maps.event.addDomListener(window, "load", initialize);
        </script>
        ';

        $output .= '<div id="'.$map_name.'" style="'.$add_height.'"></div>';

        return $output;

    }
}

add_shortcode( 'stamina_gmap', 'stamina_google_map_shortcode' ); ?>