<?php if ( ! function_exists( 'stamina_portfolio_shortcode' ) )
{
    function stamina_portfolio_shortcode( $atts )
    {
        extract( shortcode_atts( array(
            'columns'           => 'col-md-4 col-sm-6',
            'posts_per_page'    => 6,
            'pagination'        => '',
            'filter'            => true,
            'show_date'         => 'show',
            'show_author'       => 'show',
            'show_read_more'    => 'show',
            'orderby'           => 'ID',
            'cat'               => '',
            'el_class'          => '',
        ), $atts ) );

        $args = array(
            'post_type'     => 'st_portfolio',
            'posts_per_page'=> $posts_per_page,
            'orderby'       => $orderby,
            'post_status'   => 'publish',
            'cat'           => $cat,
        );

        // Classes
        $classes = array(
            'blog-posts',
            $el_class,
        );

        $classes = trim( implode( ' ', array_filter( array_unique( $classes ) ) ) );


        $categories = get_categories( array(
            'taxonomy' => 'st_portfolio_cat',
            'hide_empty'=> 1
        ) );

        $portfolio_query = new WP_Query( $args );

        ob_start(); ?>

        <div class="<?php echo esc_attr( $classes ); ?>">

            <div class="row">

                <?php if ( $portfolio_query->have_posts() ) : ?>

                    <?php if ( isset( $filter ) && ! empty( $filter ) ): ?>

                        <?php if ( ! empty( $categories ) && count( $categories ) > 1 ) : ?>

                            <!-- Works filter -->
                            <ul class="work filters">
                                <li class="filter" data-filter="all"><?php esc_html_e( 'All', 'stamina-functions' ); ?></li>

                                <?php
                                foreach ( $categories as $category ): ?>
                                    <li class="filter" data-filter=".st_portfolio_cat-<?php echo esc_html( $category->slug ); ?>"><?php echo esc_html( $category->name ); ?></li>
                                <?php endforeach; ?>
                            </ul>
                            <!-- / Works filter -->
                        <?php endif; ?>

                    <?php endif; ?>

                    <div class="portfolio">

                        <div class="row work-items">

                            <?php while ( $portfolio_query->have_posts() ) : $portfolio_query->the_post();?>

                                <?php if ( has_post_thumbnail() ):


                                ?>
                                    <div class="<?php echo esc_attr( $columns ); ?>">
                                        <!-- work item -->
                                        <div <?php post_class('mix'); ?>>
                                            <div class="item">
                                                <a href="<?php echo esc_url( get_the_post_thumbnail_url( get_the_ID() ) ); ?>" class="work-popup">
                                                    <?php the_post_thumbnail( 'stamina-portfolio-grid' ); ?>
                                                    <div class="overlay">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            <?php endwhile; ?>

                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>

    <?php
        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;
    }
}

add_shortcode( 'stamina_portfolio', 'stamina_portfolio_shortcode' ); ?>