<?php if ( ! function_exists( 'stamina_icon_box_shortcode' ) )
{
    function stamina_icon_box_shortcode( $atts )
    {
        extract( shortcode_atts( array(
            'icon'          => '',
            'title'         => '',
            'iconbox_content'=> '',
            'icon_color'    => '#0f8bcf',
            'icon_position' => 'center',
            'text_color'    => '',
            'text_position' => 'center',
            'el_class'      => '',
        ), $atts ) );

        if ( function_exists( 'stamina_icon_box_inline_css' ) ) {
            stamina_icon_box_inline_css( 'service-box', $icon_color, $icon_position, $text_color, $text_position );
        }

        // Classes
        $css_classes = array(
            'service-box',
            $icon_position,
            $text_position,
            $el_class,
        );

        $classes = trim( implode( ' ', array_filter( array_unique( $css_classes ) ) ) );

        ob_start(); ?>

        <div class="<?php echo esc_attr( $classes ); ?>">
            <?php if ( ! empty( $icon ) ): ?>
                <i class="<?php echo esc_attr( $icon ); ?>"></i>
            <?php endif; ?>
            <?php if ( ! empty( $title ) ): ?>
                <h3 class="subtitle"><?php echo esc_html( $title ); ?></h3>
            <?php endif; ?>
            <?php if ( ! empty( $iconbox_content ) ): ?>
                <p><?php echo esc_html( $iconbox_content ); ?></p>
            <?php endif; ?>
        </div>

        <?php

        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;

    }
}

add_shortcode( 'stamina_icon_box', 'stamina_icon_box_shortcode' ); ?>