<?php if ( ! function_exists( 'stamina_team_member_shortcode' ) )
{
    function stamina_team_member_shortcode( $atts )
    {
        extract( shortcode_atts( array(
            'image'         => '',
            'name'          => '',
            'role'          => '',
            'rss'           => '',
            'email'         => '',
            'facebook'      => '',
            'twitter'       => '',
            'google_plus'   => '',
            'linkedin'      => '',
            'skype'         => '',
            'el_class'      => '',
        ), $atts ) );

        // Classes
        $classes = array(
            'team-member',
            $el_class,
        );

        $classes = trim( implode( ' ', array_filter( array_unique( $classes ) ) ) );

        ob_start(); ?>

        <div class="<?php echo esc_attr( $classes ); ?>">
            <div class="team-photo">

                <?php if ( ! empty( $image ) ):
                    echo wp_get_attachment_image( $image, 'stamina-team-member-thumb' );

                    stamina_team_social_links( $rss, $facebook, $twitter, $google_plus, $linkedin, $skype, $email );
                endif; ?>
            </div>
            <div class="designation">
                <?php if ( ! empty( $name ) ): ?>
                    <h3 class="subtitle"><?php echo esc_html( $name ); ?></h3>
                <?php endif; ?>
                <?php if ( ! empty( $role ) ): ?>
                    <p class="text-muted"><?php echo esc_html( $role ); ?></p>
                <?php endif; ?>
            </div>
        </div>

        <?php

        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;

    }
}

add_shortcode( 'stamina_team_member', 'stamina_team_member_shortcode' ); ?>