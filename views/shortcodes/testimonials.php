<?php if ( ! function_exists( 'stamina_testimonials_shortcode' ) )
{
    function stamina_testimonials_shortcode( $atts )
    {
        extract( shortcode_atts( array(
            'posts_per_page'  => '',
            'orderby'         => 'ID',
            'order'           => '',
            'el_class'        => '',
        ), $atts ) );

        $args = array(
            'post_type'     => 'st_testimonial',
            'posts_per_page'=> $posts_per_page,
            'orderby'       => $orderby,
            'order'         => $order,
        );

        $custom_query = new WP_Query( $args );

        ob_start(); ?>

        <?php if ( $custom_query->have_posts() ) : ?>
            <div class="<?php echo esc_attr( $el_class ); ?>">
                <?php while ( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
                    <div class="single-testimonial text-center">
                        <i class="fa fa-quote-left"></i>
                        <?php the_content(); ?>
                        <?php if ( has_post_thumbnail() ): ?>
                            <?php the_post_thumbnail( 'stamina-testimonial-thumb' ); ?>
                        <?php endif; ?>
                        <?php
                        $author_name = get_post_meta( get_the_ID(), 'author_name', true );
                        $author_position = get_post_meta( get_the_ID(), 'author_position', true );
                        ?>

                        <?php if ( ! empty( $author_name ) ): ?>
                            <h4><?php echo esc_html( $author_name ); ?></h4>
                        <?php endif; ?>
                        <?php if ( ! empty( $author_position ) ): ?>
                            <p class="desg"><?php echo esc_html( $author_position ); ?></p>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            </div>
    <?php endif; ?>

        <?php

        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;

    }
}

add_shortcode( 'stamina_testimonials', 'stamina_testimonials_shortcode' ); ?>