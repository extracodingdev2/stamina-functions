<?php if ( ! function_exists( 'stamina_blog_grid_shortcode' ) )
{
    function stamina_blog_grid_shortcode( $atts )
    {
        extract( shortcode_atts( array(
            'columns'           => 'col-md-4 col-sm-6',
            'content_type'      => 'excerpt',
            'posts_per_page'    => 3,
            'pagination'        => '',
            'show_date'         => 'show',
            'show_author'       => 'show',
            'show_read_more'    => 'show',
            'orderby'           => 'ID',
            'cat'               => '',
            'el_class'          => '',
        ), $atts ) );

        $args = array(
            'post_type'     => 'post',
            'posts_per_page'=> $posts_per_page,
            'orderby'       => $orderby,
            'post_status'   => 'publish',
            'cat'           => $cat,
        );

        // Classes
        $classes = array(
            'blog-posts',
            $el_class,
        );

        $classes = trim( implode( ' ', array_filter( array_unique( $classes ) ) ) );

        $blog_posts = new WP_Query( $args );

        ob_start(); ?>

        <div class="<?php echo esc_attr( $classes ); ?>">
            <div class="row">
                <?php if ( $blog_posts->have_posts() ) : ?>

                    <?php while ( $blog_posts->have_posts() ) : $blog_posts->the_post(); ?>
                        <div class="<?php echo esc_attr( $columns ); ?>">
                            <div class="single-news">
                                <?php if ( has_post_thumbnail() ): ?>
                                    <div class="news-image">
                                       <?php the_post_thumbnail( 'stamina-blog-grid-image' ); ?>
                                    </div>
                                <?php endif; ?>

                                <div class="news-content">
                                    <p class="news-meta text-muted">
                                        <?php if ( 'show' == $show_author ): ?>
                                            <span><i class="fa fa-user"></i><?php echo stamina_get_user_name( get_the_ID() ); ?></span>
                                        <?php endif; ?>
                                        <?php if ( 'show' == $show_date ): ?>
                                            <span>
                                                <i class="fa fa-calendar"></i>
                                                <time itemprop="datePublished" datetime="<?php echo get_the_date( 'c' ); ?>"><?php the_time( get_option( 'date_format' ) ); ?></time>
                                            </span>
                                        <?php endif; ?>
                                    </p>

                                     <?php the_title( '<h3 class="subtitle">', '</h3>' ); ?>

                                    <?php if ( $content_type == 'content' ) {
                                        the_content();
                                     } elseif ( $content_type == 'excerpt' ) { ?>
                                            <p><?php echo stamina_get_excerpt( 263, get_the_ID() ); ?></p>
                                    <?php } ?>

                                    <?php if ( $show_read_more != 'hide' ) : ?>
                                        <a class="btn" href="<?php echo esc_url( get_the_permalink() ); ?>">
                                            <?php echo esc_html__( 'Read More', 'stamina' ); ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php
                    endwhile;
                endif; ?>
            </div>
        </div>

    <?php
        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;
    }
}

add_shortcode( 'stamina_blog_grid', 'stamina_blog_grid_shortcode' ); ?>